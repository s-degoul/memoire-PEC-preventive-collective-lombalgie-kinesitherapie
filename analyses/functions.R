
## Concatenate dir name and file name with "/"
paste.d <- function(...){
    return(paste(..., sep = "/"))
}

## Table considering NA
table.w.NA <- function(..., useNA = "ifany"){
    tab <- table(..., useNA = useNA)
    row.names(tab)[is.na(names(tab))] <- "NA"
    if(length(dim(tab)) == 2){
        colnames(tab)[is.na(colnames(tab))] <- "NA"
    }
    return(tab)
}

## Customized summary of quantitative variable
summary.num <- function(x, useNA = "always"){
    if (! sum(! is.na(x))){
        result <- c(rep(NA, 7), length(x))
    } else {
        result <- c(mean(x, na.rm = TRUE),
                    sd(x, na.rm = TRUE),
                    min(x, na.rm = TRUE),
                    quantile(x, probs = .25, names = FALSE, na.rm = TRUE),
                    median(x, na.rm = TRUE),
                    quantile(x, probs = .75, names = FALSE, na.rm = TRUE),
                    max(x, na.rm = TRUE),
                    sum(is.na(x)))
    }
    names(result) <- c("moyenne", "DS", "min", "Q1", "médiane" , "Q3", "max", "NAs")
    if (useNA == "no"
        | (useNA == "ifany" & result["NAs"] == 0)){
        return(result[-which(names(result) == "NAs")])
    }
    return(result)
}

## Numeric summary of data
summary.table <- function(x, useNA = "ifany"){
    if (class(x)[1] %in% c("numeric", "integer")){
        return(summary.num(x, useNA = useNA))
    }
    tab <- table.w.NA(x, useNA = useNA)
    tab.p <- paste0("(", round(prop.table(tab) * 100, 1), " %)")
    result <- paste(tab, tab.p)
    names(result) <- names(tab)
    return(result)
}

## Display binary data
display.binary <- function(x, label.true = "oui", label.false = "non"){
    if (class(x) != "logical"){
        x <- as.logical(x)
    }
    return(ifelse(is.na(x),
                  "NA",
           ifelse(x,
                  label.true,
                  label.false)))
}

## Display NA
display.NA <- function(x){
    return(ifelse(is.na(x),
                  "NA",
                  x))
}

## Cat with separator = ""
cat0 <- function(..., newlines = TRUE){
    newlines.str <- ""
    if (newlines){
        newlines.str <- "\n\n"
    }
    cat(paste0(newlines.str, ..., newlines.str))
}

## Safely transform POSIXct date to Date avoiding tz concerns (cf https://stackoverflow.com/questions/17098862/as-dateas-posixct-gives-the-wrong-date)
as.Date.safe <- function(d, ...){
    if (class(d)[1] != "POSIXct"){
        return(as.Date(d, ...))
    }
    return(as.Date(format(d, "%Y-%m-%d"), ...))
}

######
## Plotting with ggplot2

library(ggplot2)

## Plot distribution of quantitative variable
plot.distr.quanti <- function(data, variable){
    plt1 <- ggplot(data = data,
                   mapping = aes_string(x = variable)) +
        geom_histogram() +
        labs(x = "", y = "effectif")
    plt2 <- ggplot(data = data,
                   mapping = aes_string(x = variable)) +
        geom_boxplot() +
        labs(x = "", y = "") +
        theme(
            axis.ticks.y = element_blank(),
            axis.text.y = element_blank())
    print(cowplot::plot_grid(plt1, plt2,
                             ncol = 1, rel_heights = c(2, 1),
                             align = "v", axis = "lr"))
}

## Plot distribution of logical variable
plot.distr.logical <- function(data, variable){
    tab <- table(data[,variable], useNA = "always")
    names(tab) <- display.binary(names(tab))
    tab.df <- as.data.frame(tab)
    names(tab.df) <- c("modality", "count")
    tab.df$prop <- prop.table(tab) * 100
    print(ggplot(data = tab.df,
                 mapping = aes(x = "", y = count, fill = modality)) +
          geom_bar(stat = "identity", width = 1) +
          coord_polar("y", start = 0) +
          ## theme_void() +
          labs(x = NULL, y = NULL, fill = NULL) +
          scale_fill_manual(values = color.types$binary) +
          theme(
              axis.title.x = element_blank(),
              axis.title.y = element_blank(),
              panel.border = element_blank(),
              axis.ticks = element_blank()))
}

## Plot distribution of factor variable
plot.distr.factor <- function(data, variable, nchar.limit.angle = 8){
    angle <- 0
    if (sum(! is.na(data[,variable])) > 0){
        if (max(nchar(as.character(data[,variable])), na.rm = TRUE) > nchar.limit.angle) {
            angle <- 45
        }
    }
    print(ggplot(data = data,
                 mapping = aes_string(x = variable)) +
          geom_bar(stat = "count") +
          labs(x = "", y = "effectif") +
          theme(axis.text.x = element_text(angle = angle)))
}
