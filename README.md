# Présentation

Mémoire de kinésithérapie - État des lieux de la prise en charge collective et préventive des lombalgies en masso-kinésithérapie

Enquête de pratique réalisée auprès des masseurs kinésithérapeutes du Grand Est, à l'aide d'un questionnaire en ligne.

- Auteur : J Eltzer, étudiante à l'institut de masso-kinésithérapie d'Alsace
- Date : novembre - décembre 2021
- Langue : français

Analyses statistiques réalisées avec le langage [`R`](https://cran.r-project.org/), rapports d'analyse générés avec `rmarkdown` *via* le logiciel [`RStudio`](https://www.rstudio.com/).

# Ressources disponibles

## Répertoire `data`

- `data.csv` : données obtenues après *data management* des données exportées du questionnaire, avec notamment :
  - transformation des questions à choix multiples en autant de variables binaires que d'items
  - anonymisation des données : suppression des champs de texte libre
- `data.RData` : même données, mais au format *RData* (permet de conserver le typage des variables) : c'est ce fichier que le script d'analyse utilise
- `variables_description.csv` : description des variables disponibles, selon 3 champs :
  - *Variable* : nom de la variable
  - *Description* : description synthétique
  - *Type* : type de variable (en langage R). NB : les variables de type *character* correspondent aux questions à choix multiples et n'existent en fait pas telles quelles dans les données (cf. infra)
- `variables_modalities.csv` : description des variables binaires (type *logical*) correspondant à chaque items des questions à choix multiples :
  - *VariableOrig* : nom de la variable relative à la question
  - *VariableBin* : complément ajouté à *VariableOrig* pour obtenir le nom complet de la variable apparaissant dans les données
  - *Description* : description synthétique

## Répertoire `analyses`

- `config.R` : paramètres de configuration, en langage `R`
- `functions.R` : fonctions utilisées dans les scripts, en langage `R`
- `analyses.Rmd` : script d'analyses statistiques, en langage `rmarkdown`
- `analyses.html` : rapport d'analyses statistiques généré, au format `HTML`
- `custom_Rmarkdown_emph.css` : feuille de style CSS pour le rapport d'analyse
